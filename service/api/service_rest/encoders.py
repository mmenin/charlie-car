from common.json import ModelEncoder
from .models import Technician, Appointment



class TechnicianEncoder(ModelEncoder):
    model = Technician
    properties = [
        "name",
        "employee_number",
    ]

class AppointmentEncoder(ModelEncoder):
    model = Appointment
    properties = [
        "id",
        "vin",
        "owner",
        "date",
        "time",
        "technician",
        "service",
        "vip",
        "completed",
    ]
    encoders = {
        "technician": TechnicianEncoder()
    }
