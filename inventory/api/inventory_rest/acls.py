import json
import requests
import os

SERPAPI_KEY = os.environ["SERPAPI_KEY"]


def get_img_url(manufacturer, model):
    headers = {"Authorization": SERPAPI_KEY}
    params = {
        "q": f"{model}+{manufacturer}",
        "api_key": SERPAPI_KEY,
        "tbm": "isch",
    }
    url = "https://serpapi.com/search.json?"
    response = requests.get(url, params=params, headers=headers)
    content = json.loads(response.content)
    try:
        return content["images_results"][0]["thumbnail"]
    except (KeyError, IndexError):
        return None


