import React from 'react';


class AppointmentForm extends React.Component {

        state = {
            vin: '',
            owner: '',
            date: '',
            time: '',
            technicians: [],
            service: '',
        };

    async componentDidMount() {
        const url = 'http://localhost:8080/api/technicians/';
        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json()
            this.setState({technicians: data.technicians})
        }
        else {
            console.log('Could not load technician data')
        }
    }

     handleSubmit = async(event) => {
        this.setState({saving: true});
        event.preventDefault();
        const data = {...this.state};
        delete data.saving;
        delete data.technicians;

        const appointmentUrl = 'http://localhost:8080/api/appointments/';
        const fetchConfig = {
            method: 'post',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            }
        };
        const response = await fetch(appointmentUrl, fetchConfig);
        console.log(response)
        if (response.ok) {
            const cleared = {
            vin: '',
            owner: '',
            date: '',
            time: '',
            technician: '',
            service: '',
            saving: false,
            };
            this.setState(cleared);
        }
        else {
            console.log('Could not create appointment, check input fields and try again')
        }
    }

    handleChange = (event) => {
        const value = event.target.value;
        this.setState({ [event.target.name]: value})
    }

    render() {
        return (
            <div className="row">
            <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
                <h1> Create Service Appointment</h1>
                <form onSubmit={this.handleSubmit} id="create-appointment-form">
                <div className="form-floating mb-3">
                    <input onChange={this.handleChange} value={this.state.vin} placeholder="vin" required type="text" name="vin" id="vin" className="form-control" />
                    <label htmlFor="vin">Vin</label>
                </div>
                <div className="form-floating mb-3">
                    <input onChange={this.handleChange} value={this.state.owner} placeholder="owner" required type="text" name="owner" id="owner" className="form-control" />
                    <label htmlFor="owner">Owner</label>
                </div>
                <div className="form-floating mb-3">
                    <input onChange={this.handleChange} value={this.state.date} placeholder="date" required type="date" name="date" id="date" className="form-control" />
                    <label htmlFor="date">Date</label>
                </div>
                <div className="form-floating mb-3">
                    <input onChange={this.handleChange} value={this.state.time} placeholder="time" required type="time" name="time" id="time" className="form-control" />
                    <label htmlFor="time">Time</label>
                </div>
                <div className="mb-3">
                    <select onChange={this.handleChange} value={this.state.technician} required id="technician" name="technician" className="form-select">
                        <option value="technician">Assign a Technician</option>
                        {this.state.technicians?.map(technician => {
                            return (
                                <option key={technician.employee_number} value={technician.employee_number}>
                                    {technician.name}
                                </option>
                            )
                        })}
                    </select>
                </div>
                <div className="form-floating mb-3">
                    <input onChange={this.handleChange} value={this.state.service} placeholder="service" required type="text" name="service" id="service" className="form-control" />
                    <label htmlFor="service">Service</label>
                </div>
                <button className="btn btn-primary">Create Appointment</button>
                </form>
            </div>
            </div>
        </div>
        )
    }

}

export default AppointmentForm;
