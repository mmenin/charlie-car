import React, { useState } from 'react';

function CustomerForm(){
    const[name,setName] = useState('');
    const[address,setAdd] = useState('');
    const[phone,setPhone] = useState('');
    const handleNameChange = (e) =>{ setName(e.target.value); }
    const handleAddChange = (e) =>{ setAdd(e.target.value); }
    const handlePhoneChange = (e) => {setPhone(e.target.value); }

    const submitForm = async () => {   
        let data = {
            name: name,
            address: address,
            phone: phone
        }
        const customerUrl ='http://localhost:8090/api/customers/';
        const fetchConfig = {
            method: 'post',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(customerUrl, fetchConfig);
        if (response.ok) {
            setName('');
            setAdd('');
            setPhone(0);
         }
        else {console.log("Unable to create customer record")}
      }

    return (
        <div className="row">
            <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
                <h1> Add a Customer</h1>
                <p></p>
                <div className="form-floating mb-3">
                    <input onChange={handleNameChange} value={name} placeholder="name"
                            required type="text" name="name" id="name" className="form-control" />
                    <label htmlFor="name">Name</label>
                </div>
                <div className="form-floating mb-3">
                    <input onChange={handleAddChange} value={address} placeholder="address"
                            required type="text" name="address" id="address" className="form-control" />
                    <label htmlFor="address">Address</label>
                </div>
                <div className="form-floating mb-3">
                    <input onChange={handlePhoneChange} value={phone} placeholder="phone"
                            required type="number" name="phone" id="phone" className="form-control" />
                    <label htmlFor="phone">Phone number</label>
                </div>
                <button onClick={submitForm} className="btn btn-primary">Create</button>
            </div>
            </div>
        </div>
    )
}


export default CustomerForm;
