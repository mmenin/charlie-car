import React from 'react';


class ManufacturerForm extends React.Component {

        state = {
            name: '',
        };

    handleSubmit = async(event) => {
        this.setState({saving: true});
        event.preventDefault();
        const data = {...this.state};
        delete data.saving;

        const manufacturerUrl ='http://localhost:8100/api/manufacturers/';
        const fetchConfig = {
            method: 'post',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(manufacturerUrl, fetchConfig);
        if (response.ok) {
            const cleared = {
                name: '',
                saving: false,
            };
            this.setState(cleared);
        }
        else {
            console.log('Could not add manufacturer')
        }
    }

    handleChange = (event) => {
        const value = event.target.value;
        this.setState({[event.target.name]: value})
    }

    render() {
        return (
            <div className="row">
                <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1> Add a Manufacturer</h1>
                    <form onSubmit={this.handleSubmit} id="create-manufacturer-form">
                    <div className="form-floating mb-3">
                        <input onChange={this.handleChange} value={this.state.name} placeholder="name" required type="text" name="name" id="name" className="form-control" />
                        <label htmlFor="name">Name</label>
                    </div>
                    <button className="btn btn-primary">Create</button>
                    </form>
                </div>
                </div>
            </div>
        )
    }

}

export default ManufacturerForm;
